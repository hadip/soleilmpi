%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% INITIAL HOMOGENEOUS ISOTROPIC TURBULENCE GENERATOR %%%%%%%%
% This script uses the Passot-Pouquet spectrum to generate an
% initial turbulent field corresponding to a STAGGERED GRID.
% The result is stored in separate binary files U.bin, V.bin,
% and W.bin.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all;
clear all;
clc;

format long;

% Size of the grid
% The grid is assumed to be staggered NxNxN
N=32;

% wavenumber with maximum energy
k0=6;

% velocity scaling
u0=1;

coef=16*sqrt(2/pi)*u0^2/k0/(2*pi*k0^2);

delta=2*pi/N;

k1=linspace(-N/2,N/2-1,N);
k2=linspace(-N/2,N/2-1,N);
k3=linspace(-N/2,N/2-1,N);

k_p1=sin(k1*delta/2)./(delta/2);
k_p2=sin(k2*delta/2)./(delta/2);
k_p3=sin(k3*delta/2)./(delta/2);

V_hat_mag=zeros(N,N,N);
V_hat_mag_im=zeros(N,N,N);
V_hat_mag_re=zeros(N,N,N);
u_hat=zeros(N,N,N);
v_hat=zeros(N,N,N);
w_hat=zeros(N,N,N);

for i=1:N
    for j=1:N
        for k=1:N
            k_mag=sqrt(k1(i)^2+k2(j)^2+k3(k)^2);
            V_hat_mag(i,j,k)=sqrt(coef*(k_mag/k0)^2*exp(-2*(k_mag/k0)^2));
        end
    end
end

V1=zeros(3);
V2=zeros(3);
V_rand=zeros(3);

theta_rand=random('uniform',0,2*pi,N,N,N,3);
V_hat_mag_re=V_hat_mag.*cos(theta_rand(:,:,:,1));
V_hat_mag_im=V_hat_mag.*sin(theta_rand(:,:,:,1));

for i=2:N/2+1
    for j=2:N
        for k=2:N
            V1=cross([1.12345858 -0.9824347293487 1.18234827347374],[k_p1(i) k_p2(j) k_p3(k)]);
            V1=V1./norm(V1);
            V2=cross(V1,[k_p1(i) k_p2(j) k_p3(k)]./norm([k_p1(i) k_p2(j) k_p3(k)]));
            V_rand=V1.*cos(theta_rand(i,j,k,2))+V2.*sin(theta_rand(i,j,k,2));
            u_hat(i,j,k)=V_rand(1)*V_hat_mag_re(i,j,k);
            v_hat(i,j,k)=V_rand(2)*V_hat_mag_re(i,j,k);
            w_hat(i,j,k)=V_rand(3)*V_hat_mag_re(i,j,k);
            
            V_rand=V1.*cos(theta_rand(i,j,k,3))+V2.*sin(theta_rand(i,j,k,3));
            u_hat(i,j,k)=u_hat(i,j,k)+1i*V_rand(1)*V_hat_mag_im(i,j,k);
            v_hat(i,j,k)=v_hat(i,j,k)+1i*V_rand(2)*V_hat_mag_im(i,j,k);
            w_hat(i,j,k)=w_hat(i,j,k)+1i*V_rand(3)*V_hat_mag_im(i,j,k);
            
            u_hat(i,j,k)=u_hat(i,j,k)*exp(-1i*k1(i)*delta/2);
            v_hat(i,j,k)=v_hat(i,j,k)*exp(-1i*k2(j)*delta/2);
            w_hat(i,j,k)=w_hat(i,j,k)*exp(-1i*k3(k)*delta/2);
           
        end
    end
end


for i=N/2+2:N
    for j=2:N
        for k=2:N
            u_hat(i,j,k)=u_hat(N+2-i,N+2-j,N+2-k)';
            v_hat(i,j,k)=v_hat(N+2-i,N+2-j,N+2-k)';
            w_hat(i,j,k)=w_hat(N+2-i,N+2-j,N+2-k)';
           
        end
    end
end
i=N/2+1;
   for j=N/2+2:N
        for k=2:N
            u_hat(i,j,k)=u_hat(N+2-i,N+2-j,N+2-k)';
            v_hat(i,j,k)=v_hat(N+2-i,N+2-j,N+2-k)';
            w_hat(i,j,k)=w_hat(N+2-i,N+2-j,N+2-k)';
           
        end
    end
i=N/2+1;
   j=N/2+1;
        for k=N/2+2:N
            u_hat(i,j,k)=u_hat(N+2-i,N+2-j,N+2-k)';
            v_hat(i,j,k)=v_hat(N+2-i,N+2-j,N+2-k)';
            w_hat(i,j,k)=w_hat(N+2-i,N+2-j,N+2-k)';
           
        end
i=N/2+1;
   j=N/2+1;
        k=N/2+1;
            u_hat(i,j,k)=0;
            v_hat(i,j,k)=0;
            w_hat(i,j,k)=0;
           
   
E=0.5*sum(sum(sum(abs(u_hat.^2)+abs(v_hat.^2)+abs(w_hat.^2))))

U = ifftn( ifftshift(u_hat) )*N^3;
V = ifftn( ifftshift(v_hat) )*N^3;
W = ifftn( ifftshift(w_hat) )*N^3;

error_u=max(max(max(abs(imag(U)))))
error_v=max(max(max(abs(imag(V)))))
error_w=max(max(max(abs(imag(W)))))



U=real(U);
V=real(V);
W=real(W);

div_max=0;

for i=2:N-1
    for j=2:N-1
        for k=2:N-1
            div=(U(i+1,j,k)-U(i,j,k))+(V(i,j+1,k)-V(i,j,k))+(W(i,j,k+1)-W(i,j,k));
            if (abs(div)>div_max)
                div_max=div;
            end
        end
    end
end

div_max

u_rms=sqrt(mean(mean(mean(U.^2./3+V.^2./3+W.^2./3))))

fid_U = fopen('U.bin', 'w');
fid_V = fopen('V.bin', 'w');
fid_W = fopen('W.bin', 'w');
fwrite(fid_U,U,'double');
fwrite(fid_V,V,'double');
fwrite(fid_W,W,'double');
